# flavor_demo
A new Flutter project showcasing how to approach flavors:
    - Different environments for our applications / separate configurations for each use case
    - Same codebase

## Libs
https://pub.dev/packages/flutter_flavorizr


## Commands
    # Inital Config
    - flutter pub run flutter_flavorizr
    # Development
    - flutter run --flavor tasmu -t lib/main-tasmu.dart
    - flutter run --flavor urbiotica -t -lib/main-urbiotica.dart
    # Compilation
    - flutter build apk --flavor tasmu lib/main-tasmu.dart --no-shrink
    - flutter build apk --flavor urbiotica lib/main-urbiotica.dart --no-shrink


## Articles 

flutter_flavorizr
https://pierre-dev.hashnode.dev/get-the-best-out-of-flutter-flavors-with-flutterflavorizr

flutter_native_splash 
(Splash_screen Support in a project setup that contains multiple flavors or environments)
https://pub.dev/packages/flutter_native_splash#flavor-support
