// ignore_for_file: constant_identifier_names
enum Flavor {
  TASMU,
  URBIOTICA,
}

class F {
  static Flavor? appFlavor;

  static String get name => appFlavor?.name ?? '';

  static String get title {
    switch (appFlavor) {
      case Flavor.TASMU:
        return 'ParkCtrl Tasmu';
      case Flavor.URBIOTICA:
        return 'ParkCtrl Urbiotica';
      default:
        return 'title';
    }
  }

  static String get drawerIcon {
    switch (appFlavor) {
      case Flavor.TASMU:
        return "assets/icon_tasmu.png";
      case Flavor.URBIOTICA:
        return "assets/icon_urbiotica.png";
      default:
        throw Exception("Unknown flavor for DrawerIcon");
    }
  }

  static String get server {
    switch (appFlavor) {
      case Flavor.TASMU:
        return "http://tasmu.urbiotica.org:32507/";
      case Flavor.URBIOTICA:
        return "https://uadmin.urbiotica.net/";
      default:
        throw Exception("Unknown flavor for Server");
    }
  }
}
